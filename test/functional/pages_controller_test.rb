require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get work" do
    get :work
    assert_response :success
  end

  test "should get donate" do
    get :donate
    assert_response :success
  end

  test "should get partners" do
    get :partners
    assert_response :success
  end

end
